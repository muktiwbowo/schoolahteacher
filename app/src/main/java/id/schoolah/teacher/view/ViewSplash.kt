package id.schoolah.teacher.view

interface ViewSplash: ViewBase {
    fun showHome()
    fun showLogin()
}