package id.schoolah.teacher.service

import id.schoolah.teacher.controller.ControllerPreference

class ServicePreference(private val controllerPreference: ControllerPreference) {

    var isLogin: Boolean?
        get() {
            return controllerPreference.getPreference("is_login", false) as Boolean
        }
        set(value) {
            value?.let {
                controllerPreference.setPreference("is_login", it)
            }
        }

    var sessionToken: String?
        get() {
            return controllerPreference.getPreference("session_token", "")
                .toString()
        }
        set(value) {
            value.let {
                controllerPreference.setPreference("session_token", it.toString())
            }
        }

    fun clearSession() {
        controllerPreference.clearPreference("is_login")
    }
}