package id.schoolah.teacher.activity

import android.content.Intent
import android.os.Bundle
import id.schoolah.teacher.R
import id.schoolah.teacher.presenter.PresenterSplash
import id.schoolah.teacher.view.ViewSplash
import javax.inject.Inject

class ActivitySplash : ActivityBase(), ViewSplash {

    @Inject lateinit var presenterSplash: PresenterSplash

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initPresenter()
    }

    private fun initPresenter(){
        componentActivity?.injectActivity(this)
        presenterSplash.attachView(this)
        presenterSplash.onViewCreated()
    }

    override fun onResume() {
        super.onResume()
        presenterSplash.startSplash()
    }

    override fun onStop() {
        super.onStop()
        presenterSplash.endSplash()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenterSplash.destroyView()
    }

    override fun showHome() {
        startActivity(Intent(this, ActivityHome::class.java))
        finish()
    }

    override fun showLogin() {
        startActivity(Intent(this, ActivityLogin::class.java))
        finish()
    }
}
