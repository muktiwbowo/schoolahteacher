package id.schoolah.teacher.injection.module

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import dagger.Module
import dagger.Provides
import id.schoolah.teacher.injection.scope.ScopeActivity

@Module
class ModuleActivity(private val activity: AppCompatActivity) {

    @Provides
    @ScopeActivity
    fun providesActivity(): AppCompatActivity = activity

    @Provides
    @ScopeActivity
    fun providesFragmentManager(): FragmentManager = activity.supportFragmentManager
}