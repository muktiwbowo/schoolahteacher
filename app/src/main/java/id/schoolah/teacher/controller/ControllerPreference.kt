package id.schoolah.teacher.controller

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import id.schoolah.teacher.util.UtilConstant

class ControllerPreference(application: Application) {
    private var sharedPreferences: SharedPreferences =
        application.getSharedPreferences(UtilConstant.TEACHER_PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun setPreference(key: String, value: Any){
        val editor = sharedPreferences.edit()

        when(value){
            is String -> editor.putString(key, value)
            is Int -> editor.putInt(key, value)
            is Boolean -> editor.putBoolean(key, value)
        }

        editor.apply()
    }

    fun getPreference(key: String, value: Any): Any{
        return when(value) {
            is Int -> sharedPreferences.getInt(key, value)
            is Boolean -> sharedPreferences.getBoolean(key, value)
            else -> sharedPreferences.getString(key, value.toString()).toString()
        }
    }

    fun clearPreference(key: String){
        val editor = sharedPreferences!!.edit()
        editor.remove(key)
        editor.apply()
    }
}