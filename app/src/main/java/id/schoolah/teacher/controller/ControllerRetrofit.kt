package id.schoolah.teacher.controller

import id.schoolah.teacher.service.ServiceApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ControllerRetrofit {
    fun instanceRetrofit(): ServiceApi{
        return Retrofit.Builder()
            .baseUrl("https://staging-api.schoolah.id/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ServiceApi::class.java)
    }
}