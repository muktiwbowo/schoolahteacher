package id.schoolah.teacher.injection.module

import android.app.Application
import dagger.Module
import dagger.Provides
import id.schoolah.teacher.controller.ControllerPreference
import id.schoolah.teacher.controller.ControllerRetrofit
import id.schoolah.teacher.injection.scope.ScopeApplication
import id.schoolah.teacher.service.ServiceApi
import id.schoolah.teacher.service.ServicePreference

@Module
class ModuleApplication(private val application: Application) {

    @Provides
    @ScopeApplication
    fun providesApplication(): Application = application

    @Provides
    @ScopeApplication
    fun providesPreference(controllerPreference: ControllerPreference): ServicePreference{
        return ServicePreference(controllerPreference)
    }

    @Provides
    @ScopeApplication
    fun providesControllerPreference(application: Application): ControllerPreference{
        return ControllerPreference(application)
    }

    @Provides
    @ScopeApplication
    fun providesRetrofit(): ServiceApi{
        return ControllerRetrofit().instanceRetrofit()
    }
}