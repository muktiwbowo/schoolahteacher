package id.schoolah.teacher.controller

import android.app.Application
import id.schoolah.teacher.injection.component.ComponentApplication
import id.schoolah.teacher.injection.component.DaggerComponentApplication
import id.schoolah.teacher.injection.module.ModuleApplication

class ControllerApplication: Application() {
    companion object{
        lateinit var componentApplication: ComponentApplication
    }

    override fun onCreate() {
        super.onCreate()
        componentApplication = DaggerComponentApplication
            .builder()
            .moduleApplication(ModuleApplication(this))
            .build()
    }
}