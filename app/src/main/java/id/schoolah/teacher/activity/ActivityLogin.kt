package id.schoolah.teacher.activity

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.view.View
import androidx.appcompat.app.AlertDialog
import id.schoolah.teacher.R
import id.schoolah.teacher.presenter.PresenterLogin
import id.schoolah.teacher.view.ViewLogin
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.toast
import javax.inject.Inject

class ActivityLogin : ActivityBase(), ViewLogin, View.OnClickListener {

    @Inject lateinit var presenterLogin: PresenterLogin

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initPresenter()
        initView()
    }

    private fun initPresenter(){
        componentActivity?.injectLogin(this)
        presenterLogin.attachView(this)
    }

    private fun initView(){
        login_button_sign_in.setOnClickListener(this)
        login_button_register.setOnClickListener(this)
        login_icon_google.setOnClickListener(this)
        login_icon_facebook.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.login_button_sign_in ->{
                val username = SpannableString(login_form_username.text).toString()
                val password = SpannableString(login_form_password.text).toString()
                presenterLogin.responseLogin(username, password)
            }
            R.id.login_button_register ->{
                startActivity(Intent(this, ActivityRegister::class.java))
            }
            R.id.login_icon_google ->{
                toast("Fitur ini akan tersedia saat release")
            }
            R.id.login_icon_facebook ->{
                toast("Fitur ini akan tersedia saat release")
            }
        }
    }

    override fun showLoginStatus(message: String) {
        toast(message)
    }

    override fun showLoginToHome() {
        startActivity(Intent(this, ActivityHome::class.java))
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenterLogin.destroyView()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(this)
            .setMessage("Yakin keluar dari aplikasi?")
            .setPositiveButton("Ya") { _, _ ->
                finish()
            }
            .setNegativeButton("Tidak") { dialog, _ ->
                dialog.dismiss()
            }
            .create().show()
    }
}
