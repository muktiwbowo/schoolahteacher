package id.schoolah.teacher.injection.scope

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(value = AnnotationRetention.RUNTIME)
annotation class ScopeApplication