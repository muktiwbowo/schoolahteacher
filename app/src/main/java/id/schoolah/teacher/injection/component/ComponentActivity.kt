package id.schoolah.teacher.injection.component

import dagger.Component
import id.schoolah.teacher.activity.ActivityLogin
import id.schoolah.teacher.activity.ActivitySplash
import id.schoolah.teacher.injection.module.ModuleActivity
import id.schoolah.teacher.injection.scope.ScopeActivity

@ScopeActivity
@Component(dependencies = [ComponentApplication::class], modules = [ModuleActivity::class])
interface ComponentActivity {
    fun injectActivity(activitySplash: ActivitySplash)
    fun injectLogin(activityLogin: ActivityLogin)
}