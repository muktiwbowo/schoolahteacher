package id.schoolah.teacher.injection.component

import dagger.Component
import id.schoolah.teacher.injection.module.ModuleApplication
import id.schoolah.teacher.injection.scope.ScopeApplication
import id.schoolah.teacher.service.ServiceApi
import id.schoolah.teacher.service.ServicePreference

@ScopeApplication
@Component(modules = [ModuleApplication::class])
interface ComponentApplication {
    fun servicePreference(): ServicePreference
    fun serviceApi(): ServiceApi
}