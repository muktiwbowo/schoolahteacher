package id.schoolah.teacher.presenter

import android.os.Handler
import android.util.Log
import id.schoolah.teacher.service.ServicePreference
import id.schoolah.teacher.view.ViewSplash
import javax.inject.Inject

class PresenterSplash @Inject constructor(private val servicePreference: ServicePreference) : PresenterBase<ViewSplash>() {
    private lateinit var runnable: Runnable
    private var handler: Handler = Handler()

    fun onViewCreated() {
        goToNextActivity()
    }

    fun startSplash() {
        handler.postDelayed(runnable, 1800)
    }

    fun endSplash() {
        handler.removeCallbacks(runnable)
    }

    private fun goToNextActivity() {
        Log.e("PresenterSplash", "${servicePreference.isLogin}")
        runnable = Runnable {
            if (servicePreference.isLogin as Boolean) {
                viewBase?.showHome()
            }else{
                viewBase?.showLogin()
            }
        }
    }
}