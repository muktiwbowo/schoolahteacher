package id.schoolah.teacher.view

interface ViewLogin: ViewBase {
    fun showLoginStatus(message: String)
    fun showLoginToHome()
}