package id.schoolah.teacher.activity

import androidx.appcompat.app.AppCompatActivity
import id.schoolah.teacher.controller.ControllerApplication
import id.schoolah.teacher.injection.component.ComponentActivity
import id.schoolah.teacher.injection.component.DaggerComponentActivity

open class ActivityBase: AppCompatActivity() {
    val componentActivity: ComponentActivity? = null
    get() {
        checkNotNull(field){
            return DaggerComponentActivity
                .builder()
                .componentApplication(ControllerApplication.componentApplication)
                .build()
        }
        return field
    }
}