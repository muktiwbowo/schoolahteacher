package id.schoolah.teacher.service

import com.google.gson.JsonObject
import id.schoolah.teacher.model.ModelLogin
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ServiceApi {
    @POST("v1/users/login")
    fun requestLogin(@Body jsonObject: JsonObject): Call<ModelLogin>
}