package id.schoolah.teacher.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelLogin (
    @SerializedName("code") var codeStatus: String?,
    @SerializedName("token") var token: String?
): Parcelable