package id.schoolah.teacher.presenter

import android.util.Log
import com.google.gson.JsonObject
import id.schoolah.teacher.model.ModelLogin
import id.schoolah.teacher.service.ServiceApi
import id.schoolah.teacher.service.ServicePreference
import id.schoolah.teacher.view.ViewLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PresenterLogin
@Inject constructor(
    private val serviceApi: ServiceApi,
    private val servicePreference: ServicePreference): PresenterBase<ViewLogin>() {

    val PRESENTER_TAG = PresenterLogin::class.java.simpleName

    fun responseLogin(username: String, password: String){
        val jsonObject = JsonObject()
        jsonObject.addProperty("username", username)
        jsonObject.addProperty("password", password)

        serviceApi.requestLogin(jsonObject).enqueue(object : Callback<ModelLogin>{
            override fun onResponse(call: Call<ModelLogin>, response: Response<ModelLogin>) {
                if (response.isSuccessful && response.body()?.codeStatus == "201"){
                    servicePreference.sessionToken = response.body()?.token
                    servicePreference.isLogin = true
                    viewBase?.showLoginStatus("Successfully login")
                    viewBase?.showLoginToHome()
                    Log.e(PRESENTER_TAG, "onSuccess: ${response.body()?.codeStatus}")
                }else{
                    viewBase?.showLoginStatus("Silahkan coba lagi")
                    Log.e(PRESENTER_TAG, "onGoneWrong: ${response.body()?.codeStatus}")
                }
            }

            override fun onFailure(call: Call<ModelLogin>, t: Throwable) {
                viewBase?.showLoginStatus("Login gagal, cek koneksi internet anda")
                Log.e(PRESENTER_TAG, "onFailure: ${t.localizedMessage}")
            }
        })
    }
}