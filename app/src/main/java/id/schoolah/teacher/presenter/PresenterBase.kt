package id.schoolah.teacher.presenter

import id.schoolah.teacher.view.ViewBase

abstract class PresenterBase<T: ViewBase>  {
    var viewBase: T? = null

    open fun attachView(view: T){
        viewBase = view
    }

    fun destroyView(){
        viewBase = null
    }
}